gfx/blaster/orange_particle
{  
  cull disable
  {
    map gfx/blaster/orange_particle.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
    rgbGen vertex
  }
}

gfx/blaster/blue_particle
{  
  cull disable
  {
    map gfx/blaster/blue_particle.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
    rgbGen vertex
  }
}

gfx/lcannon/white_particle
{  
  cull disable
  {
    map gfx/lcannon/white_particle.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
    rgbGen vertex
  }
}

gfx/mdriver/green_particle
{  
  cull disable
  {
    map gfx/mdriver/green_particle.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/mdriver/trail
{
  nomipmaps
  cull disable
  {
    map gfx/mdriver/trail.tga
    blendFunc blend
  }
}

gfx/mdriver/mdriver
{  
  cull disable
  {
    map gfx/mdriver/mdriver.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/psaw/blue_particle
{  
  cull disable
  {
    map gfx/psaw/blue_particle.jpg
    blendFunc GL_ONE GL_ONE
    alphaGen vertex
    rgbGen vertex
  }
}

gfx/rifle/verysmallrock
{  
  cull disable
  {
    map gfx/rifle/verysmallrock.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
    rgbGen vertex
  }
}

gfx/rifle/purple_trail
{  
  nomipmaps
  cull disable
  {
    map gfx/rifle/purple_streak.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
  }
}

gfx/prifle/red_blob
{  
  cull disable
  {
    animmap 24 gfx/prifle/pr1.jpg gfx/prifle/pr2.jpg gfx/prifle/pr3.jpg gfx/prifle/pr4.jpg
		blendFunc GL_ONE GL_ONE
  }
}

gfx/prifle/red_streak
{  
  nomipmaps
  cull disable
  {
    map gfx/prifle/red_streak.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
  }
}

gfx/lcannon/primary
{  
  cull disable
  {
    animmap 24 gfx/lcannon/primary_1.jpg gfx/lcannon/primary_2.jpg gfx/lcannon/primary_3.jpg gfx/lcannon/primary_4.jpg
		blendFunc GL_ONE GL_ONE
  }
}

gfx/lgun/blue_blob
{  
  cull disable
  {
    map gfx/lasgun/blue_blob.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
    rgbGen vertex
  }
}

gfx/lasgun/purple_particle
{  
  cull disable
  {
    map gfx/lasgun/purple_particle.tga
		blendFunc GL_ONE GL_ONE
  }
}

gfx/lasgun/purple_trail
{  
  nomipmaps
  cull disable
  {
    map gfx/lasgun/purple_streak.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    alphaGen vertex
  }
}

gfx/blood/grenadesmokepuff
{
  cull disable
  {
    map gfx/blood/grenadesmokepuff.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/blood/smokepuff3
{
  cull disable
  {
    map gfx/blood/smokepuff3.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/shotgun/smokeshotgun
{
  cull disable
  {
    map gfx/shotgun/smokeshotgun.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/blood/csmoke
{
  cull disable
  {
    map gfx/blood/csmoke.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/flame/fire
{
  cull disable
  {
    map gfx/flame/fire.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/blood/grenadesmoke.tga
{
  cull disable
  {
    map gfx/blood/grenadesmoke.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/blood/impactpuff2
{
  cull disable
  {
    map gfx/blood/impactpuff2.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/blood/impactpuff3.tga
{
  cull disable
  {
    map gfx/blood/impactpuff3.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/rifle/concreteparticle2
{
  cull disable
  {
    map gfx/rifle/concreteparticle2.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/mdriver/purple_trail
{
  cull disable
  {
    map gfx/mdriver/purple_trail.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/prifle/primary
{  
  cull disable
  {
    animmap 24 gfx/prifle/pr1.jpg gfx/prifle/pr2.jpg gfx/prifle/pr3.jpg gfx/prifle/pr4.jpg
		blendFunc GL_ONE GL_ONE
  }
}

gfx/blood/green_acid2
{
  cull disable
  {
    map gfx/blood/green_acid2.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}
gfx/weapons/flash
{
  cull none
  entityMergable
  {
    map gfx/weapons/flash.tga
    blendFunc add
    rgbGen vertex
    alphaGen vertex
  }
}
gfx/weapons/smoke
{
  cull none
  entityMergable
  {
    map gfx/weapons/smoke.tga
    blendFunc blend
    rgbGen vertex
    alphaGen vertex
  }
}
gfx/grenade/smoke
{
  cull none
  entityMergable
  {
    map gfx/weapons/smoke.tga
    blendFunc blend
    rgbGen vertex
    alphaGen vertex
  }
}
gfx/grenade/puff
{
  //nopicmip
  cull disable
  entityMergable
  //deformVertexes wave 40 sin 0 0.1 0 0.5
  {
    map gfx/weapons/puff.tga
    blendFunc blend
    rgbGen vertex
    alphaGen vertex
  }
  {
    map gfx/weapons/puffstreak.tga
    blendfunc blend
    rgbGen vertex
    alphaGen vertex

    tcMod turb 0 0.05 0 0.5
    tcMod scroll -0.5 0.0
  }
  {
    map gfx/weapons/fire.tga
    blendfunc blend
    rgbGen vertex
    alphaGen vertex
    tcMod turb 0 0.05 0 0.5
    tcMod scroll -1 0
  }
}
