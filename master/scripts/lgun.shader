models/weapons/lgun/lgun
{
	cull disable
	{
		map models/weapons/lgun/lgun.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
	{
		map models/weapons/lgun/lgun_glow.jpg
		blendFunc add
		rgbGen wave triangle .9 .1 0 0.5
		detail
	}
	{
		map models/weapons/lgun/lgun_effect.tga
		blendfunc add
		rgbGen wave noise 0 1 0 10 
	}
	{
		map models/weapons/lgun/lgun_effect0.tga
		blendfunc add
		rgbGen identityLighting
	}
	{
		animmap 6 models/weapons/lgun/lgun_tray0.tga models/weapons/lgun/lgun_tray1.tga models/weapons/lgun/lgun_tray2.tga models/weapons/lgun/lgun_tray3.tga models/weapons/lgun/lgun_tray4.tga models/weapons/lgun/lgun_tray5.tga models/weapons/lgun/lgun_tray6.tga models/weapons/lgun/lgun_tray7.tga 
		blendfunc add
		rgbGen wave inversesawtooth 0 1 0 1 
	}
}
