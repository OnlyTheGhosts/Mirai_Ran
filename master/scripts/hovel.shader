models/buildables/hovel/hovel
{
	{
		map models/buildables/hovel/hovel.tga
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/acid_tube/effect.tga
		blendfunc add
		rgbGen wave triangle 0.1 0.05 0 0.1 
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
}

models/buildables/hovel/hovel_front
{
	{
		map models/buildables/hovel/hovel_front.tga
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/acid_tube/effect.tga
		blendfunc add
		rgbGen wave triangle 0.1 0.05 0 0.2
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
}
