models/buildables/trapper/trapper_gills
{
cull disable
	{
		map models/buildables/trapper/trapper_gills.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
	}
	{
		map models/buildables/trapper/trapper_gills.tga
		rgbGen lightingDiffuse
		tcMod scroll -0.01 0
		tcMod scale -1 1
		alphaFunc GE128
	}
}
models/buildables/trapper/trapper_body
{
	{
		map models/buildables/trapper/trapper_body.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/acid_tube/effect.tga
		blendfunc add
		rgbGen wave triangle 0.1 0.1 0 0.2 
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
}
models/buildables/trapper/trapper_tent
{
	cull disable
	{
		map models/buildables/trapper/trapper_tent.tga
		rgbGen lightingDiffuse
	}
	{
		map models/buildables/acid_tube/effect.tga
		blendfunc add
		rgbGen wave triangle 0.1 0.1 0 0.2 
	}
	{
		map models/buildables/trapper/trapper_tent_effect.tga
		blendfunc add
		rgbGen wave triangle 0.1 0.1 0 0.2  
	}
}
