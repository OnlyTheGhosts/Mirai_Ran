models/weapons/grenade/grenade
{
	cull disable
	{
		map models/weapons/grenade/grenade.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
}

models/weapons/grenade/grenade_s
{
	{
		map models/weapons/grenade/energy.jpg
		rgbGen wave square 0 1 0 2
		tcMod scale 2 1
	}
}

gfx/grenade/flare_01
{
	{
		map gfx/grenade/flare_01.tga
		blendfunc add
	}
}

grenadeExplosion
{
	cull disable
	{
		animmap 5 models/weaphits/glboom/glboom_1.tga  models/weaphits/glboom/glboom_2.tga models/weaphits/glboom/glboom_3.tga
		rgbGen wave inversesawtooth 0 1 0 5
		blendfunc add
	}
	{
		animmap 5 models/weaphits/glboom/glboom_2.tga  models/weaphits/glboom/glboom_3.tga gfx/colors/black.tga
		rgbGen wave sawtooth 0 1 0 5
		blendfunc add
	}
}

grenadepuff
{
  beam
  {
    shader		gfx/weapons/smoke
    segments		16
    width		10 128
    alpha 		1.0 0.0
    segmentTime		10
    fadeOutTime		1200
    textureType		stretch 1.0 0.0

    //jitter 1 10
  }
}

models/weapons/grenade/impactTrailPS2
{
  ejector
  {
    particle
    {
      shader sync gfx/weapons/smoke

      displacement 0 0 -40 ~60

      velocityType      static
      velocityDir       linear
      velocityMagnitude 5
      velocity          0 0 1 ~360

      radius 0 105.0~75% 65.0~75%
      alpha  0 0.6 0.0
      rotation 0 ~360 -
      bounce 0

      //overdrawProtection 

      lifeTime 5000
    }

    count 4
    delay 50
    period 40 - 0
  }
}
models/weapons/grenade/pufffire
{
  ejector
  {
    particle
    {
      shader sync flamer3 flamer4 flamer5 flamer6 flamer7 flamer8 flamer9 flamer10 flamer11 flamer12 flamer13 flamer14 flamer15 flamer16 flamer17 flamer18 flamer19 flamer20 flamer21 flamer22 flamer23

      displacement 0 0 0 ~3

      velocityType      static
      velocityDir       linear
      velocityMagnitude 5
      velocity          0 0 1 ~360

      radius 0 5.0~75% 5.0~75%
      alpha  0 1.0 0.0
      rotation 0 ~360 -~2%
      bounce 0

      lifeTime 100
    }

    count 5
    delay 0
    period 50 - 0
  }
}

models/weapons/grenade/impactPS
{
  ejector
  {
    particle
    {
      shader sync flamer3 flamer4 flamer5 flamer6 flamer7 flamer8 flamer9 flamer10 flamer11 flamer12 flamer13 flamer14 flamer15 flamer16 flamer17 flamer18 flamer19 flamer20 flamer21 flamer22 flamer23

      displacement 0 0 11 ~10

      velocityType      static
      velocityDir       linear
      velocityMagnitude 150~75%
      velocity          0 0 1 ~45

      accelerationType      static
      accelerationDir       linear
      accelerationMagnitude 10
      acceleration          0 0 -1 0

      radius 0 25.0~75% 75.0~75%
      alpha  500 1.0 0.0
      rotation 0 ~360 -
      bounce 0

      lifeTime 500

      childSystem models/weapons/grenade/impactTrailPS2
    }

    count 2
    delay 0
    period 0 - ~0%
  }
  ejector
  {
    particle
    {
      shader sync gfx/sprites/spark

      displacement		0 0 0 ~0

      velocityType		static
      velocityDir		linear
      velocityMagnitude		750
      velocity			0 0 1 ~90

      accelerationType		static
      accelerationDir   	linear
      accelerationMagnitude	1200
      acceleration		0 0 -1 0

      radius 0 50 15
      alpha 0 1.0 1.0
      bounce 0
      rotation 0 0 50
      lifeTime 250

      //childSystem models/weapons/grenade/pufffire
      childTrailSystem		grenadepuff
    }
   
    count 10~50%
    delay 0
    period 0 1 4~10%
  }
  ejector
  {
    particle
    {
      shader sync gfx/grenade/flare_01

      displacement 0 0 8 ~0

      velocityType      static
      velocityDir       linear
      velocityMagnitude 0
      velocity          0 0 1 ~60

      radius 0 0 650
      alpha  0 1.0 0.0
      rotation 0 ~360 -
      bounce 0.0

      lifeTime 450
    }

    count 1
    delay 0
    period 0 - ~0%
  }

  ejector
  {
    particle
    {
      shader sync gfx/sprites/spark

      displacement 0 0 50 ~175

      velocityType      static
      velocityDir       linear
      velocityMagnitude 20
      velocity          0 0 -1 ~20

      accelerationType      static
      accelerationDir       linear
      accelerationMagnitude 50
      //acceleration          -0.5~1 -0.5~1 1 ~180
      acceleration          0 0 1 ~180

      radius 0 15.0 2.0
      alpha  500 1.0 0.0
      rotation 0 ~360 -
      bounce 0

      lifeTime 3000

      //childTrailSystem		grenadeflare
    }

    count 50
    delay 350
    period 0 - ~150
  }
}
