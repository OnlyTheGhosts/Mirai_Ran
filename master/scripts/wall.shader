textures/wall/stein_2
{
	qer_editorimage textures/wall/stein_2.tga
	surfaceparm bricks
	{
		map $lightmap
		rgbgen identity
	}
	{
		map textures/wall/stein_2.tga
		blendfunc gl_dst_color gl_zero
		rgbgen identity
	}
}

textures/wall/stein_3a
{
	qer_editorimage textures/wall/stein_3a.tga
	surfaceparm bricks
	{
		map $lightmap
		rgbgen identity
	}
	{
		map textures/wall/stein_3a.tga
		blendfunc gl_dst_color gl_zero
		rgbgen identity
	}
}

textures/wall/stein_7a
{
	qer_editorimage textures/wall/stein_7a.tga
	surfaceparm bricks
	{
		map $lightmap
		rgbgen identity
	}
	{
		map textures/wall/stein_7a.tga
		blendfunc gl_dst_color gl_zero
		rgbgen identity
	}
}