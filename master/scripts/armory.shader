models/buildables/arm/arm_body
{
	{
		map models/buildables/arm/arm_body.jpg
		rgbGen lightingSpecular
	}
	{
		map $whiteimage
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/arm/arm_bodyglow.tga
		blendfunc add
		rgbGen wave sin 0 0.5 0.6 0.1
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		rgbGen lightingDiffuse
		detail
		tcGen environment
	}
}

models/buildables/arm/arm_parts
{
	{
		map models/buildables/arm/arm_parts.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		tcGen environment
	}
}

models/buildables/arm/arm_panel1
{
	{
		map models/buildables/arm/arm_panel1.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/arm/arm_panel1_glow.jpg
		blendFunc add
		rgbGen wave sin .98 .02 0 5
		detail
	}
}

models/buildables/arm/arm_panel2
{
	{
		map models/buildables/arm/arm_panel2.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/mgturret/arm_panel2_glow.jpg
		blendFunc add
		rgbGen wave sin .98 .02 0 5
		detail
	}
}

models/buildables/arm/arm_panel3
{
	{
		map models/buildables/arm/arm_panel3.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/arm/arm_panel3_glow.jpg
		blendFunc add
		rgbGen wave sin .98 .02 0 5
		detail
	}
}
