textures/snow/snow
{
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nolightmap
	qer_trans .5
	deformVertexes move 3 1 0 sin 0 5 0 0.2
	cull none

	{
		map textures/snow/snow.tga
		tcMod Scroll .5 -.5
		tcMod turb .1 .25 0 -.1
		blendFunc GL_ONE GL_ONE
	}
	{
		map textures/snow/snow.tga
		tcMod Scroll .5 -1
		blendFunc GL_ONE GL_ONE
	}
}

textures/snow/snow2
{
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
	cull disable
	deformVertexes move 3 1 0 sin 0 5 0 0.2
	qer_editorimage textures/snow/snow.tga
	qer_trans 0.5

	{
		map textures/snow/snow.tga
		blendfunc add
		rgbGen identity
		tcMod Scroll .5 -.5
		tcMod turb .1 .25 0 -.1
	}
}

textures/niveus/snow_02
{
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
	cull disable
	qer_editorimage textures/niveus/snow_02.jpg
	qer_trans 0.5

	{
		map textures/niveus/snow_02.jpg
		blendfunc add
		rgbGen identity
		tcMod Scroll .5 -.05
		tcMod turb .1 .25 0 -.1
	}
}

textures/snow/snow3
{
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
	cull disable
	deformVertexes move 3 1 0 sin 0 5 0 0.2
	qer_editorimage textures/snow/snow.tga
	qer_trans 0.5

	{
		map textures/snow/snow.tga
		blendfunc add
		rgbGen identity
		tcMod Scroll 2 -2
		tcMod turb .1 .25 0 -.1
	}
}

textures/snow/snow4
{
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
	cull disable
	deformVertexes move 3 1 0 sin 0 5 0 0.2
	qer_editorimage textures/snow/snow.tga
	qer_trans 0.5

	{
		map textures/snow/snow.tga
		blendfunc add
		rgbGen identity
		tcMod Scroll .9 -.9
		tcMod turb .1 .45 0 -.1
	}
}

textures/snow/snow5
{
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
	cull disable
	deformVertexes move 3 1 0 sin 0 5 0 0.2
	qer_editorimage textures/snow/snow.tga
	qer_trans 0.5

	{
		map textures/snow/snow.tga
		blendfunc add
		rgbGen identity
		tcMod Scroll .1 -.9
		tcMod turb .1 .1 0 -.1
	}
}