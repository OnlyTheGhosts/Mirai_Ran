gfx/marks/blood2
{
  polygonOffset
  {
    map gfx/blood/blood2.tga
    blendFunc blend
    rgbGen identityLighting
    alphaGen vertex
  }
}

gfx/marks/ablood
{
  polygonOffset
  {
    map gfx/blood/green_acid2.tga
    blendFunc blend
    rgbGen identityLighting
    alphaGen vertex
  }
}
