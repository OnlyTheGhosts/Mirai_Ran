models/players/human_base/jetpack
{
	cull disable
	{
		map models/players/human_base/jetpack.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
		depthFunc equal
	}
	{
		map models/players/human_base/jetpack_glow.jpg
		rgbGen wave square 0 1 0 1
		blendFunc add
		depthFunc equal
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
		depthFunc equal
	}
}

models/players/human_base/jetpack_flash
{
	sort additive
	{
		map	models/players/human_base/jetpack_flash.jpg
		blendfunc GL_ONE GL_ONE
		tcMod scroll 10.0 0.0
	}
}
