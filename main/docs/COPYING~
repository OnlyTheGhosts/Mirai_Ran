---------------------------------------------------------- Tremulous License ---

Tremulous is licensed in two broadly separate sections: the code and the media. 

The code is licensed under the GNU GENERAL PUBLIC LICENSE. This license is
contained in full in the file named GPL. Please be aware of the exceptions to
this license as listed below.

The media is licensed under the CREATIVE COMMONS ATTRIBUTION-SHAREALIKE 2.5
LICENSE. Please read http://creativecommons.org/licenses/by-sa/2.5/ to learn
more about this license. The full license text is contained in the file named
CC.

----------------------------------------------------------- Mimod License ---

The media created for Mimod is liscensed under the CREATIVE COMMONS 
ATTRIBUTION-SHAREALIKE 2.5 LICENSE.

JPEG library
-----------------------------------------------------------------------------
src/jpeg-6
Copyright (C) 1991-1995, Thomas G. Lane

Permission is hereby granted to use, copy, modify, and distribute this
software (or portions thereof) for any purpose, without fee, subject to these
conditions:
(1) If any part of the source code for this software is distributed, then this
README file must be included, with this copyright and no-warranty notice
unaltered; and any additions, deletions, or changes to the original files
must be clearly indicated in accompanying documentation.
(2) If only executable code is distributed, then the accompanying
documentation must state that "this software is based in part on the work of
the Independent JPEG Group".
(3) Permission for use of this software is granted only if the user accepts
full responsibility for any undesirable consequences; the authors accept
NO LIABILITY for damages of any kind.

These conditions apply to any software derived from or based on the IJG code,
not just to the unmodified library.  If you use our work, you ought to
acknowledge us.

NOTE: unfortunately the README that came with our copy of the library has
been lost, so the one from release 6b is included instead. There are a few
'glue type' modifications to the library to make it easier to use from
the engine, but otherwise the dependency can be easily cleaned up to a
better release of the library.

CURL library
-----------------------------------------------------------------------------
src/curl-7.12.2
COPYRIGHT AND PERMISSION NOTICE

Copyright (c) 1996 - 2004, Daniel Stenberg, <daniel@haxx.se>.

All rights reserved.

Permission to use, copy, modify, and distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright
notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

FT2 library
-----------------------------------------------------------------------------
src/ft2
The FT2 library is being used under the GPL v2 as indicated by its LICENSE.txt

PNG library
-----------------------------------------------------------------------------
code/png
Copyright (c) 2004, 2006 Glenn Randers-Pehrson

Permission is hereby granted to use, copy, modify, and distribute this
source code, or portions hereof, for any purpose, without fee, subject
to the following restrictions:

1. The origin of this source code must not be misrepresented.

2. Altered versions must be plainly marked as such and must not
   be misrepresented as being the original source.

3. This Copyright notice may not be removed or altered from any
   source or altered source distribution.

The Contributing Authors and Group 42, Inc. specifically permit, without
fee, and encourage the use of this source code as a component to
supporting the PNG file format in commercial products.  If you use this
source code in a product, acknowledgment is not required but would be
appreciated.

The PNG Reference Library is supplied "AS IS".  The Contributing Authors
and Group 42, Inc. disclaim all warranties, expressed or implied,
including, without limitation, the warranties of merchantability and of
fitness for any purpose.  The Contributing Authors and Group 42, Inc.
assume no liability for direct, indirect, incidental, special, exemplary,
or consequential damages, which may result from the use of the PNG
Reference Library, even if advised of the possibility of such damage.

ZLIB library
-----------------------------------------------------------------------------
code/zlib
(C) 1995-2004 Jean-loup Gailly and Mark Adler

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

----------------------------------------------------------- Emoticons ---

This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 
3.0 United States License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/us/ 
or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
 
Edit and redistribute at your discretion. Credit must be given to the original creator.

Michael Grudziecki
mcgrudzi@gmail.com

--------------------------------------------------- Flag Emoticons ---

Flag icons - http://www.famfamfam.com

These icons are public domain, and as such are free for any use (attribution appreciated but not required).

Note that these flags are named using the ISO3166-1 alpha-2 country codes where appropriate. A list of codes can be found at http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2

If you find these icons useful, please donate via paypal to mjames@gmail.com (or click the donate button available at http://www.famfamfam.com/lab/icons/silk)

Contact: mjames@gmail.com

--------------------------------------------------- OS Emoticons ---
Most OS emoticons fall under this license:

QEmu OS Icons, version 0.1

Creator of this package: Marc Reichelt, http://marcreichelt.de/
License:                 LGPL (GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html)


Copyright and Trademark information:
Some logos are copyrighted and/or are registered trademarks of certain companys or groups.
The icons may be used in neutral context, e.g. as desktop icons for an emulator like QEmu.

All other OS icons are copyrighted their individual owners.

--------------------------------------------------- Standard Emoticons ---

Standard emoticons are all from Pidgin, another GPL project. We do not own these icons.

--------------------------------------------------- All Other Emoticons ---

All other emoticons are owned by their individual owners. Those that have not been given 
to us are under the 'fair use' section of the DMCA.

--------------------------------------------------- Code License Exceptions ---

The following files contain sections of code that are not licensed under the
GPL, but are nevertheless GPL compatible. The license text for these licenses
is contained within the files as listed.

    src/qcommon/unzip.c                                         zlib license
    src/game/bg_lib.c                                            BSD license
    src/client/snd_adpcm.c            Stichting Mathematisch Centrum license
    src/jpeg-6/*                                                JPEG license
    src/ogg_vorbis/*                                             OGG license
