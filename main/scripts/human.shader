models/players/human_base/h_base
{
	cull disable
	{
		map models/players/human_base/h_base.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map models/players/human_base/h_base.tga
		blendFunc GL_SRC_ALPHA GL_ONE
		detail
		alphaGen lightingSpecular
		depthFunc equal
	}
}

models/players/human_base/h_helmet
{
	cull disable
	{
		map models/players/human_base/h_helmet.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map models/players/human_base/hexagonal_shields_helmet.tga
		blendfunc add
		rgbGen wave triangle 1 1 0 0.3 
	}
	{
		map models/players/human_base/viseur.tga
		blendfunc blend
		alphaGen wave inversesawtooth 0 1 0 0.5 
	}
	{
		map models/players/human_base/ref_helmet.tga
		blendFunc GL_SRC_ALPHA GL_ONE
		detail
		tcGen environment
		alphaGen lightingSpecular
		depthFunc equal
	}
}

models/players/human_base/battpack
{
	cull disable
	{
		map models/players/human_base/battpack.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		models/players/human_base/battpack.tga
		blendFunc GL_SRC_ALPHA GL_ONE
		detail
		alphaGen lightingSpecular
		depthFunc equal
	}
	{
		map models/players/human_base/battpack_effect.tga
		blendfunc add
		rgbGen wave noise 0 1 0 10 
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
		depthFunc equal
	}
}

models/players/human_base/shoulderpads
{
	cull disable
	{
		map models/players/human_base/shoulderpads.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map models/players/human_base/shoulderpads_glow.tga
		rgbGen wave triangle 0.5 0.02 0 1
		blendFunc add
		depthFunc equal
	}
	{
		map models/players/human_base/hexagonal_shields.tga
		blendfunc add
		rgbGen wave triangle 1 1 0 0.3 
	}
	{
		map models/players/human_base/ref_shoulderpads.tga
		blendFunc GL_SRC_ALPHA GL_ONE
		detail
		tcGen environment
		alphaGen lightingSpecular
		depthFunc equal
	}
}

models/players/human_base/armour
{
	cull disable
	{
		map models/players/human_base/armour.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map models/players/human_base/armour_glow.tga
		rgbGen wave triangle 0.5 0.02 0 1
		blendFunc add
		depthFunc equal
	}
	{
		map models/players/human_base/hexagonal_shields.tga
		blendfunc add
		rgbGen wave triangle 1 1 0 0.3 
	}
	{
		map models/players/human_base/ref_armour.tga
		blendFunc GL_SRC_ALPHA GL_ONE
		detail
		tcGen environment
		alphaGen lightingSpecular
		depthFunc equal
	}
}

models/players/human_base/light
{
	cull disable
	{
		map models/players/human_base/light.tga
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map models/players/human_base/light_glow.tga
		rgbGen wave triangle 0.5 0.02 0 1
		blendFunc add
		depthFunc equal
	}
}

models/players/human_bsuit/human_bsuit
{
	cull disable
	{
		map models/players/human_bsuit/human_bsuit.jpg
		rgbGen lightingDiffuse
		alphaFunc GE128
		depthWrite
	}
	{
		map models/players/human_bsuit/human_bsuit_effects_shields.tga
		blendfunc add
		rgbGen wave inversesawtooth 0 1 0 0.7 
	}
	{
		map models/players/human_bsuit/human_bsuit_effects.tga
		blendfunc add
		rgbGen wave triangle 0 1 0 0.1 
	}
	{
		map models/players/human_bsuit/human_bsuit_eye.tga
		blendfunc add
		rgbGen wave noise 0 1 0 10 
	}
	{
		map models/players/human_bsuit/human_bsuit_glow.tga
		rgbGen wave triangle 0.5 0.02 0 1
		blendFunc add
		depthFunc equal
	}
	{
		map models/players/human_bsuit/ref_bsuit.tga
		blendFunc GL_SRC_ALPHA GL_ONE
		detail
                tcGen environment
		alphaGen lightingSpecular
		depthFunc equal
	}
}
