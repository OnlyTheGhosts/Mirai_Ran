textures/atmosphere/raindrop
{
	cull none
	polygonoffset
	{
		map textures/atmosphere/raindrop.tga
		blendfunc GL_ONE GL_ONE
		//blendfunc blend
		//alphagen vertex
	}
}

textures/tc_atmosphere/raindropwater
{
	cull none
	{
		map textures/atmosphere/raindropwater.tga
		blendfunc blend
		alphagen vertex
	}
}

textures/atmosphere/raindropsolid
{
	polygonoffset
	{
		map textures/atmosphere/raindropsolid.tga
		blendfunc blend
		alphagen vertex
	}
}

rainSplashSimple
{
	cull disable
	{
		map models/splash/rainSplashSimple.tga
		rgbGen wave inversesawtooth 0 0.6 0 2
		blendfunc add
	}
}

rainSplash
{
	cull disable
	{
		animmap 5 models/splash/rainSplash1.tga  models/splash/rainSplash2.tga  models/splash/rainSplash3.tga gfx/colors/black.tga
		rgbGen wave inversesawtooth 0 1 0 5
		blendfunc add
	}
	{
		animmap 5 models/splash/rainSplash2.tga  models/splash/rainSplash3.tga  gfx/colors/black.tga gfx/colors/black.tga
		rgbGen wave sawtooth 0 1 0 5
		blendfunc add
	}
}

textures/atmosphere/snowflake
{
	cull none
	polygonoffset
	{
		map textures/tc_atmosphere/snowflake.tga
		blendfunc GL_ONE GL_ONE
		//blendfunc blend
		//alphagen vertex
	}
}

textures/atmosphere/snowfall1
{
      qer_editorimage textures/atmosphere/snowflakes.tga
      qer_trans .5
        
      deformVertexes move 5 0 0  sin -2 2 0 0.07
      deformVertexes move 0 5 0  sin -2 2 0 0.1
	deformVertexes autosprite2
	
      surfaceparm trans	
      surfaceparm nomarks
      surfaceparm nolightmap
        
	cull none
	{
		map textures/atmosphere/snowflakes.tga
            tcMod Scroll 0 -0.8
            blendFunc GL_ONE GL_ONE
      }
}
textures/atmosphere/snowfall2
{
        qer_editorimage textures/atmosphere/snowflakes.tga
        qer_trans .5
                
      deformVertexes move 4 1 0  sin 0 4 0 0.08
      deformVertexes move 1 4 0  sin 0 4 0 0.09
	deformVertexes autosprite2
	
        surfaceparm trans	
        surfaceparm nomarks	
        //surfaceparm nonsolid
        surfaceparm nolightmap

	cull none
        //nopicmip 
	{
		map textures/tc_atmosphere/snowflakes.tga
                //tcMod Scroll 0.05 -0.9
                tcMod Scroll 0 -0.9
                blendFunc GL_ONE GL_ONE
        }
}
textures/atmosphere/snowfall3
{
        qer_editorimage textures/atmosphere/snowflakes.tga
        qer_trans .5
                
        deformVertexes move 3 2 0  sin 0 4 0 0.115//0.65
        deformVertexes move 2 3 0  sin 0 4 0 0.145//0.45
	deformVertexes autosprite2
	
        surfaceparm trans	
        surfaceparm nomarks	
        //surfaceparm nonsolid
        surfaceparm nolightmap
	
	cull none
        //nopicmip 
	{
		map textures/atmosphere/snowflakes.tga
                //tcMod Scroll -0.07 -1.0
                tcMod Scroll 0 -1.0
                blendFunc GL_ONE GL_ONE
        }
}
textures/tc_atmosphere/snowfall4
{
        qer_editorimage textures/atmosphere/snowflakes.tga
        qer_trans .5
                
       	deformVertexes move 2 3 0  sin 0 4 0 0.075
        deformVertexes move 3 2 0  sin 0 4 0 0.13
	deformVertexes autosprite2
	
        surfaceparm trans	
        surfaceparm nomarks	
        //surfaceparm nonsolid
        surfaceparm nolightmap

	cull none
        //nopicmip 
	{
		map textures/atmosphere/snowflakes.tga
                //tcMod Scroll 0.09 -1.1
                tcMod Scroll 0 -1.1
                blendFunc GL_ONE GL_ONE
        }
}

textures/atmosphere/snow_up
{
        qer_editorimage textures/atmosphere/snowflakes.tga
        surfaceparm trans	
        surfaceparm nomarks	
        surfaceparm nonsolid
        surfaceparm nolightmap
        qer_trans .5
        
        deformVertexes move 6 5 0  sin 0 5 0 0.05
        deformVertexes move .6 3.3 0  sin 0 5 0 0.08
        deformVertexes wave 64 sin 0 10 0 0.03
	{
		map textures/atmosphere/snowflakes.tga
                tcMod turb .1 0.4 0 0.1
                tcMod scroll .01 .013
                blendFunc GL_ONE GL_ONE
        }
        
}

textures/atmosphere/snow_up2
{
        qer_editorimage textures/atmosphere/snowflakes.tga
        surfaceparm trans	
        surfaceparm nomarks	
        surfaceparm nonsolid
        surfaceparm nolightmap
        qer_trans .5
        
        deformVertexes move 5 6.2 0  sin 0 5 0 0.06
        deformVertexes move .7 2.7 0  sin 0 4 0 0.07
        deformVertexes wave 64 sin 0 10 0 0.023
	{
		map textures/atmosphere/snowflakes.tga
                tcMod turb .1 0.34 0 0.2
                tcMod scroll .011 .012
                blendFunc GL_ONE GL_ONE
        }
}

texture/atmosphere/snowflake00
{
	nopicmip
	cull none
	polygonoffset
	{
		map texture/atmosphere/snowflake00.tga
		blendfunc blend
		alphagen vertex
	}
}

texture/atmosphere/snowflake01
{
	nopicmip
	cull none
	polygonoffset
	{
		map texture/atmosphere/snowflake01.tga
		blendfunc blend
		alphagen vertex
	}
}

texture/tc_atmosphere/snowflake02
{
	nopicmip
	cull none
	polygonoffset
	{
		map texture/atmosphere/snowflake02.tga
		blendfunc blend
		alphagen vertex
	}
}

texture/atmosphere/snowflake03
{
	nopicmip
	cull none
	polygonoffset
	{
		map texture/atmosphere/snowflake03.tga
		blendfunc blend
		alphagen vertex
	}
}

texture/atmosphere/snowflake04
{
	nopicmip
	cull none
	polygonoffset
	{
		map texture/atmosphere/snowflake04.tga
		blendfunc blend
		alphagen vertex
	}
}

texture/atmosphere/snowflake05
{
	nopicmip
	cull none
	polygonoffset
	{
		map texture/atmosphere/snowflake05.tga
		blendfunc blend
		alphagen vertex
	}
}
