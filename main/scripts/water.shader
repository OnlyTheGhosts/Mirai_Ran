//deep ocean
textures/misc/dark_water
{
	{
		map textures/misc/dark_water.tga
		tcMod rotate 1
	}
	{
		map textures/misc/caustic.tga
		blendfunc add
		rgbGen wave sin 0 1 0 0.05 
		tcMod scale 0.5 0.5
		tcMod rotate 1
	}
	{
		map textures/misc/caustic.tga
		blendfunc add
		rgbGen wave sin 0 1 0 -0.05 
		tcMod scale -0.5 -0.5
		tcMod rotate 1
	}
}

//deep pool
textures/liquids/water
{
	qer_editorimage textures/liquids/water.tga
	qer_trans .5
	
	entityMergable
	
	q3map_globaltexture

	surfaceparm nobuild
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm water

	surfaceparm fog
	fogparms ( 0.01 0.05 0.05 ) 1024

	cull disable
	tesssize 128
	deformVertexes wave 100 sin 1 2 1 .1

	{
		map textures/liquids/water.tga
		blendfunc GL_ONE GL_SRC_COLOR
		tcMod scale .03 .03
		tcMod scroll .01 .001
	}
	{
		map textures/liquids/water.tga
		blendfunc GL_DST_COLOR GL_ONE
		tcMod turb .1 .1 0 .01
		tcMod scale .5 .5
		tcMod scroll -.02 .1
	}
	{
		map $lightmap
		rgbGen identity
		tcGen lightmap 
		blendfunc filter
	}
}

textures/liquids/pool3d
{
	qer_editorimage textures/liquids/pool3d_3.tga
	qer_trans .5
	surfaceparm trans
	portal
	surfaceparm nonsolid
	{
		map textures/liquids/mirror1.tga
		blendfunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
		depthWrite
	}
		{
		map textures/liquids/pool3d_5c.tga
		blendFunc GL_dst_color GL_one
		rgbgen identity
		tcmod scale .5 .5
		tcmod transform 1.5 0 1.5 1 1 2
		tcmod scroll -.05 .001
	}
	{
		map textures/liquids/pool3d_6c.tga
		blendFunc GL_dst_color GL_one
		rgbgen identity
		tcmod scale .25 .25
		tcmod transform 0 1.5 1 1.5 2 1
		tcmod scroll .025 -.001
	}
	{
		map textures/liquids/pool3d_3c.tga
		blendFunc GL_dst_color GL_one
		rgbgen identity
		tcmod scale .125 .25
		tcmod scroll .001 .025
	}
	{
		map $lightmap
		blendFunc GL_dst_color GL_zero
		rgbgen identity
	}
}

textures/glass/pool3d
{
	qer_editorimage textures/glass/mirror1.tga
	qer_trans .5
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm water
	surfaceparm nolightmap
	{
		map textures/natter/mirror1.tga
		blendfunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}
}


// Special shader for water edges
textures/misc/h20nodraw
{
	surfaceparm nodraw
	qer_trans 0.40
	surfaceparm nonsolid
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm water
	qer_editorimage textures/common/h20nodraw.tga
}

textures/misc/green_water
{
	qer_editorimage textures/misc/water_base.tga
	qer_trans .5
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm water
	
	cull disable
	deformvertexes wave 64 sin .5 .5 0 .5	
		
	{
		map $lightmap
		rgbgen identity		
	}
	{ 
		map textures/misc/water_base.tga
		blendfunc gl_dst_color gl_zero
		rgbgen identity
		tcmod scale 0.5 0.5
		tcmod transform 1.5 0 1.5 1 1 2
		tcmod scroll -.005 .001
	}
    	{
		map textures/misc/water_2.tga
            blendfunc gl_dst_color gl_one
		tcmod transform 0 1.5 1 1.5 2 1
		tcmod scroll -.01 .006
	}

}

textures/misc/green_water_fog
{
	qer_editorimage textures/tc_sfx/water_base.tga
	qer_trans .5
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm water

	surfaceparm	fog
	fogparms ( 0 0.2 0.15 ) 120
	
	cull disable
	deformvertexes wave 64 sin .5 .5 0 .5	
		
	{
		map $lightmap
		rgbgen identity		
	}
	{ 
		map textures/misc/water_base.tga
		blendfunc gl_dst_color gl_zero
		rgbgen identity
		tcmod scale 0.5 0.5
		tcmod transform 1.5 0 1.5 1 1 2
		tcmod scroll -.005 .001
	}
    	{
		map textures/misc/water_2.tga
            blendfunc gl_dst_color gl_one
		tcmod transform 0 1.5 1 1.5 2 1
		tcmod scroll -.01 .006
	}
}

textures/misc/q3tc_water
{
	qer_editorimage textures/misc/q3_pool3d_5e.tga
	qer_trans .5
	q3map_globaltexture
	surfaceparm water
	surfaceparm lava		
	surfaceparm trans
	surfaceparm nonsolid
			
	cull disable

	//surfaceparm	fog
	//fogparms ( 0 0 0 ) 768

	{ 	
		map textures/misc/q3_pool3d_5e.tga
		blendfunc gl_dst_color gl_one
		rgbgen identity
		tcmod scale .75 .75
		tcmod scroll .025 .01
	}
	{ 	
		map textures/misc/q3_pool3d_5e.tga
		blendfunc gl_dst_color gl_one
		rgbgen identity
		tcmod scale -.75 -.75
		tcmod scroll .025 .025
	}
	{
		map $lightmap
		blendfunc gl_dst_color gl_zero
		rgbgen identity		
	}
}

//tank bubbles
textures/misc/bubbles
{
	cull disable
	{
		map textures/misc/bubbles.tga
		blendfunc add
		rgbGen wave noise 0 1 0 0.02 
		tcMod scroll -0.01 0.05
		tcMod scale 2 2
	}
	{
		map textures/misc/bubbles.tga
		blendfunc add
		rgbGen wave noise 0 1 0 0.02 
		tcMod scroll 0.01 0.02
		tcMod scale -2 2
	}
}

textures/liquids/flatripplewater_1500
	{

		qer_editorimage textures/liquids/pool2.tga
		q3map_globaltexture
		q3map_lightsubdivide 32
		surfaceparm noimpact
		surfaceparm lava
		surfaceparm nolightmap
		q3map_surfacelight 500
		cull disable
		deformVertexes wave 100 sin 3 2 .1 0.1
	{
		map textures/liquids/pool2.tga
		tcMod turb 0 .2 0 .1
	}
}

textures/liquids/clear_ripple2
	{
		qer_editorimage textures/liquids/pool3d_3b.tga
		qer_trans .5
		q3map_globaltexture
		surfaceparm trans
		surfaceparm nonsolid
		surfaceparm water
		cull disable
		deformVertexes wave 64 sin .5 .5 0 .5	
		{ 
			map textures/liquids/pool3d_5b.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .5 .5
			tcmod transform 1.5 0 1.5 1 1 2
			tcmod scroll -.05 .001
		}
		{ 
			map textures/liquids/pool3d_6b.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .5 .5
			tcmod transform 0 1.5 1 1.5 2 1
			tcmod scroll .025 -.001
		}
		{ 
			map textures/liquids/pool3d_3b.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .25 .5
			tcmod scroll .001 .025
		}
		{
			map $lightmap
			blendFunc GL_dst_color GL_zero
			rgbgen identity		
		}
}

textures/liquids/clear_ripple1
	{
		qer_editorimage textures/liquids/pool3d_3.tga
		qer_trans .5
		q3map_globaltexture
		surfaceparm trans
		surfaceparm nonsolid
		surfaceparm water
		cull disable
		deformVertexes wave 64 sin .5 .5 0 .5	
		{ 
			map textures/liquids/pool3d_5.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .5 .5
			tcmod transform 1.5 0 1.5 1 1 2
			tcmod scroll -.05 .001
		}
		{ 
			map textures/liquids/pool3d_6.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .5 .5
			tcmod transform 0 1.5 1 1.5 2 1
			tcmod scroll .025 -.001
		}
		{ 
			map textures/liquids/pool3d_3.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .25 .5
			tcmod scroll .001 .025
		}	
		{
			map $lightmap
			blendFunc GL_dst_color GL_zero
			rgbgen identity		
		}
}

textures/liquids/clear_ripple1_q3dm1
	{
		qer_editorimage textures/liquids/pool3d_3.tga
		qer_trans .5
		q3map_globaltexture
		surfaceparm trans
		surfaceparm nonsolid
		surfaceparm water
		cull disable
		deformVertexes wave 64 sin .5 .5 0 .5	
		{ 
			map textures/liquids/pool3d_5.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .5 .5
			tcmod transform 1.5 0 1.5 1 1 2
			tcmod scroll -.05 .001
		}
		{ 
			map textures/liquids/pool3d_6.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .5 .5
			tcmod transform 0 1.5 1 1.5 2 1
			tcmod scroll .025 -.001
		}
		{ 
			map textures/liquids/pool3d_3.tga
			blendFunc GL_dst_color GL_one
			rgbgen identity
			tcmod scale .25 .5
			tcmod scroll .001 .025
		}	
		{
			map $lightmap
			blendFunc GL_dst_color GL_zero
			rgbgen identity		
		}
}

//foamy water top
textures/misc/foam
{
	surfaceparm nonsolid
	surfaceparm trans
	surfaceparm water
	deformVertexes wave 128 sin 0 5 6 0.5 
	tessSize 32
	cull disable
	{
		map textures/misc/foam.tga
		blendfunc add
	}
}

textures/liquids/slime1
	{
		qer_editorimage textures/liquids/slime7.tga
		q3map_lightimage textures/liquids/slime7.tga
		q3map_globaltexture
		qer_trans .5
		surfaceparm noimpact
		surfaceparm slime
		surfaceparm nolightmap
		surfaceparm trans		
		q3map_surfacelight 100
		tessSize 32
		cull disable
		deformVertexes wave 100 sin 0 1 .5 .5
		{
			map textures/liquids/slime7c.tga
			tcMod turb .3 .2 1 .05
			tcMod scroll .01 .01
		}
		{
			map textures/liquids/slime7.tga
			blendfunc GL_ONE GL_ONE
			tcMod turb .2 .1 1 .05
			tcMod scale .5 .5
			tcMod scroll .01 .01
		}
		{
			map textures/liquids/bubbles.tga
			blendfunc GL_ZERO GL_SRC_COLOR
			tcMod turb .2 .1 .1 .2
			tcMod scale .05 .05
			tcMod scroll .001 .001
		}		
}

textures/liquids/slime1_2000
	{
		qer_editorimage textures/liquids/slime7.tga
		q3map_lightimage textures/liquids/slime7.tga
		q3map_globaltexture
		qer_trans .5
		surfaceparm noimpact
		surfaceparm slime
		surfaceparm nolightmap
		surfaceparm trans		
		q3map_surfacelight 2000
		tessSize 32
		cull disable
		deformVertexes wave 100 sin 0 1 .5 .5
		{
			map textures/liquids/slime7c.tga
			tcMod turb .3 .2 1 .05
			tcMod scroll .01 .01
		}
		{
			map textures/liquids/slime7.tga
			blendfunc GL_ONE GL_ONE
			tcMod turb .2 .1 1 .05
			tcMod scale .5 .5
			tcMod scroll .01 .01
		}
		{
			map textures/liquids/bubbles.tga
			blendfunc GL_ZERO GL_SRC_COLOR
			tcMod turb .2 .1 .1 .2
			tcMod scale .05 .05
			tcMod scroll .001 .001
		}		
}

*water0
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		tcMod turb 1 0.1 0.5 0.11
		tcMod turb 1 0.1 1.0 0.11
		animMap 8  textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.25 0.25
		blendfunc blend
	}
dp_reflect 8 0.9 0.9 1 0.9
}

*water1
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.25 0.25
		blendfunc blend
	}
dp_reflect 8 1 0.75 0.25 0.5
}

*water2
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 1 1 1 0.5
}

*04awater1
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 1 1 1 0.5
}

*04mwat1
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 1 0.75 0.25 0.5
}

*04mwat2
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 1 1 1 0.5
}

*04water1
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 1 0.75 0.25 0.5
}

*04water2
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 1 0.75 0.25 0.5
}

*slime
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 0.5 1 0.75 0.5
}

*slime0
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 0.5 1 0.75 0.5
}

*slime1
{
	qer_editorimage textures/liquids/Water_16_001.tga
	qer_trans 100
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm water
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_surfacelight 1800
	{
		animMap 8 textures/liquids/Water_16_001.tga textures/liquids/Water_16_002.tga textures/liquids/Water_16_003.tga textures/liquids/Water_16_004.tga textures/liquids/Water_16_005.tga textures/liquids/Water_16_006.tga textures/liquids/Water_16_007.tga textures/liquids/Water_16_008.tga textures/liquids/Water_16_009.tga textures/liquids/Water_16_010.tga textures/liquids/Water_16_011.tga textures/liquids/Water_16_012.tga textures/liquids/Water_16_013.tga textures/liquids/Water_16_014.tga textures/liquids/Water_16_015.tga textures/liquids/Water_16_016.tga 
		tcmod scale 0.3 0.3
		blendfunc blend
	}
dp_reflect 8 0.5 1 0.75 0.5
}