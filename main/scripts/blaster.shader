models/weapons/blaster/blaster
{
	cull disable
	{
		map models/weapons/blaster/blaster.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
      {
		map models/weapons/blaster/blaster_glow.jpg
		blendFunc add
		rgbGen wave sin .98 .02 0 5
		detail
	}
	{
		map models/weapons/blaster/blaster_effect.tga
		blendfunc add
		rgbGen wave inversesawtooth 0 1 0 1 
	}
}


