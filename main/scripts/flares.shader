//**********************************
//	Flares:  MISC Flares/Light Effects
//**********************************
//		Flares are light effects that can really add to the visual aspect of your level.  These are created by making
//		a brush with the NODRAW/NONSOLID yellow texture then putting the appropriate Lens Flare texture on one side
//		of the brush. 


textures/flares/blueflare
{
    deformVertexes autoSprite
    surfaceparm trans
    surfaceparm nomarks
    surfaceparm nolightmap
    cull none
          {
            clampmap textures/flares/blueflare.tga
            blendFunc GL_ONE GL_ONE
          }
}


textures/flares/redflare
{
    deformVertexes autoSprite
    surfaceparm trans
    surfaceparm nomarks
    surfaceparm nolightmap
    cull none
          {
            clampmap textures/flares/redflare.tga
            blendFunc GL_ONE GL_ONE
          }
}

flareShader
gfx/misc/flare
{
    deformVertexes autoSprite
    surfaceparm trans
    surfaceparm nomarks
    surfaceparm nolightmap
    cull none
          {
            clampmap gfx/misc/flare.tga
            blendFunc GL_ONE GL_ONE
          }
}

textures/flares/corona
{
    deformVertexes autoSprite
    surfaceparm trans
    surfaceparm nomarks
    surfaceparm nolightmap
    cull none
          {
            clampmap textures/flares/corona.tga
            blendFunc GL_ONE GL_ONE
          }
}

//Louie "KnightBK" Doulias
//Black Knight Productions
//http://www.og-world.com

flare1
{
	cull none
	{
		map gfx/misc/flare1.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

lensflare1
{
	cull none
	{
		map gfx/misc/lensflare1.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

lensflare2
{
	cull none
	{
		map gfx/misc/lensflare2.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
//sun flares
sun
{
	cull none
	{
		map gfx/misc/sun.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

flare1_sun
{
	cull none
	{
		map gfx/misc/flare1_sun.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
flare1_sun3
{
	cull none
	{
		map gfx/misc/flare1_sun3.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
/////////NEW
WhiteRing
{
	cull none
	{
		map gfx/misc/WhiteRing.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
BlueDisc
{
	cull none
	{
		map gfx/misc/BlueDisc.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
BlueDiscWeak
{
	cull none
	{
		map gfx/misc/BlueDiscWeak.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
BrownDisc
{
	cull none
	{
		map gfx/misc/BrownDisc.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
WhiteGradient
{
	cull none
	{
		map gfx/misc/WhiteGradient.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
BrownRing
{
	cull none
	{
		map gfx/misc/BrownRing.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
BlueGradient
{
	cull none
	{
		map gfx/misc/BlueGradient.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
GreenRing
{
	cull none
	{
		map gfx/misc/GreenRing.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
RainBow
{
	cull none
	{
		map gfx/misc/RainBowRing.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}
bbox
{
	nopicmip

	{
		map gfx/misc/bbox.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

bbox_nocull
{
	nopicmip
	cull none

	{
		map gfx/misc/bbox.tga
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}


