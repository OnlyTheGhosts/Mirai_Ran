models/weapons/shotgun/shotgun
{
	cull disable
	{
		map models/weapons/shotgun/shotgun.jpg
		rgbGen lightingDiffuse
	}
	{
		map $whiteimage
		rgbGen lightingDiffuse
		blendFunc GL_DST_COLOR GL_SRC_ALPHA
		detail
		alphaGen lightingSpecular
	}
	{
	
		map models/weapons/shotgun/shotgun_glow.jpg
		blendFunc add
		rgbGen wave sin .98 .02 0 5
		detail
	}
	{
		map models/weapons/shotgun/shotgun_effect.tga
		blendfunc add
		rgbGen wave triangle 0 1 0 1 
	}
	{
		map models/buildables/mgturret/ref_map.jpg
		blendFunc GL_DST_COLOR GL_ONE
		detail
		tcGen environment
	}
}
