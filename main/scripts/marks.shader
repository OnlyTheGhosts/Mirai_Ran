// markShadow is the very cheap blurry blob underneath the player
gfx/marks/shadow
{
  polygonOffset
  {
    map gfx/marks/shadow.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

// wake is the mark on water surfaces for paddling players
gfx/marks/wake
{
  polygonOffset
  {
    clampmap gfx/marks/splash.tga
    blendFunc GL_ONE GL_ONE
    rgbGen vertex
    tcmod rotate 250
    tcMod stretch sin .9 0.1 0 0.7
    rgbGen wave sin .7 .3 .25 .5
  }
  {
    clampmap gfx/marks/splash.tga
    blendFunc GL_ONE GL_ONE
    rgbGen vertex
    tcmod rotate -230
    tcMod stretch sin .9 0.05 0 0.9
    rgbGen wave sin .7 .3 .25 .4
  }
}

gfx/marks/bullet_mrk
{
  polygonOffset
  {
    map gfx/marks/bullet_mrk.tga
    blendFunc GL_ZERO GL_ONE_MINUS_SRC_COLOR
    rgbGen exactVertex
  }
}

gfx/marks/lgun_mrk
{
  polygonOffset
  {
    map gfx/lgun/purple_particle.tga
    blendFunc GL_ONE GL_ONE
    rgbGen exactVertex
  }
}

gfx/marks/mdriver_mrk
{
  nopicmip
  polygonOffset
  {
    map gfx/blood/mdrivermk.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/marks/lcannonmissile
{
  polygonOffset
  {
    map gfx/lcannon/hotspot.tga
    blendFunc GL_ONE GL_ONE_MINUS_SRC_COLOR
    rgbGen exactVertex
  }
}

gfx/marks/burn_mrk
{
  polygonOffset
  {
    map gfx/marks/burn_mrk.tga
    blendFunc GL_ZERO GL_ONE_MINUS_SRC_COLOR
    rgbGen exactVertex
  }
}

gfx/marks/plasma_mrk
{
  polygonOffset
  {
    map gfx/marks/plasma_mrk.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

creep
{
  nopicmip
  polygonoffset
  {
    clampmap gfx/misc/creep.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen identityLighting
    alphaGen vertex
  }
 {
		map models/buildables/acid_tube/effect.tga
		blendfunc add
		rgbGen wave triangle 0.1 0.05 0 0.8 
  }
}


gfx/blood/green_acid2
{
  polygonOffset
  {
    map gfx/blood/green_acid2.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen vertex
    alphaGen vertex
  }
}

gfx/marks/rifle_mrk
{
  polygonOffset
  {
    map gfx/blood/metaldent.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen identityLighting
    alphaGen vertex
  }
}

gfx/marks/shotgun_mrk
{
  polygonOffset
  {
    map gfx/blood/shotgun.tga
    blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    rgbGen identityLighting
    alphaGen vertex
  }
}
