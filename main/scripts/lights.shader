textures/lightblock/lightblock
{
	qer_editorimage textures/lightblock/lightblock.tga
	qer_trans 0.5
	surfaceparm nonsolid
	surfaceparm nomarks
	{
		map $whiteimage
		alphaFunc GT0
		alphaGen const 0
	}
}

lights/onlywhite
{
	qer_editorimage textures/lights/onlywhite.tga
	surfaceparm nolightmap
	surfaceparm nomarks
	q3map_surfacelight 12000
	{
		map textures/lights/onlywhite.tga
		rgbGen identity
	}
}

lights/glow
{
	qer_editorimage textures/lights/glow01.tga
	qer_trans 0.5
	surfaceparm nolightmap
	surfaceparm nomarks
	deformVertexes autosprite
	surfaceparm trans
	{
		map textures/lights/glow01.tga
		blendFunc add
		rgbGen identity
	}
}